FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /src
COPY ["NumberConversion/", "NumberConversionBuild/"]
WORKDIR "/src/NumberConversionBuild"
RUN dotnet restore
COPY . .
RUN dotnet publish "NumberConversion.Web/NumberConversion.Web.csproj" -c release -o /app --no-restore

FROM node:12.16.1 AS node-build
WORKDIR /src
COPY NumberConversion/NumberConversion.Web/ClientApp .
RUN npm install --silent
RUN npm run build

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS base
WORKDIR /app
COPY --from=build /app .
COPY --from=node-build /src/dist ./ClientApp/dist
ENTRYPOINT ["dotnet", "NumberConversion.Web.dll"]
