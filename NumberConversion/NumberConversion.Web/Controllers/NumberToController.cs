﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NumberTo.NumberConversion;

namespace NumberConversion.Web.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class NumberToController : ControllerBase
    {
        private NumberConversionSoapTypeClient NumberConversionService { get; }
        
        public NumberToController(NumberConversionSoapTypeClient numberConversionService)
        {
            NumberConversionService = numberConversionService;
        }

        [HttpGet]
        public async Task<ActionResult> NumberToWords(ulong value)
        {
            var result = (await NumberConversionService.NumberToWordsAsync(value)).Body.NumberToWordsResult;
            return Ok(result);
        }
        
        [HttpGet]
        public async Task<ActionResult> NumberToDollars(decimal value)
        {
            var result = (await NumberConversionService.NumberToDollarsAsync(value)).Body.NumberToDollarsResult;
            return Ok(result);
        }
    }
}