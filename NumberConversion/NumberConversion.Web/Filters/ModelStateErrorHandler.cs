﻿using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;

namespace NumberTo.Filters
{
    public static class ModelStateErrorHandler
    {
        public static ObjectResult HandleModelStateError(ActionContext context)
        {
            var error = context.ModelState.Values
                .SelectMany(x => x.Errors
                    .Select(p => p.ErrorMessage)
                    .ToList()).FirstOrDefault();

            var result = new ObjectResult(error)
            {
                StatusCode = (int)HttpStatusCode.BadRequest
            };
            
            return result;
        }
    }
}