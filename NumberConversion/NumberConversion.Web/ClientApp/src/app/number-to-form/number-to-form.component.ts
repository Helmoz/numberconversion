import {Component, Input, OnInit} from '@angular/core';
import {RequestModel, RequestType} from '../models/RequestModel';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-number-to-form[type][color][icon][clicks]',
  templateUrl: './number-to-form.component.html'
})
export class NumberToFormComponent implements OnInit {
  constructor() { }

  value = null;

  @Input() type: RequestType;
  @Input() decimal: boolean;
  @Input() color: string;
  @Input() icon: string;
  @Input() clicks: Subject<RequestModel>;

  ngOnInit() {
  }

  sendRequest() {
    this.clicks.next({type: this.type, value: this.value});
  }
}
