export class RequestModel {
  type: RequestType;
  value: number;
}

export type RequestType = 'words' | 'dollars';
