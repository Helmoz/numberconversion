import { Component, OnInit } from '@angular/core';
import {RequestModel} from '../models/RequestModel';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {empty, Subject} from 'rxjs';
import {catchError, switchMap, tap} from 'rxjs/operators';

@Component({
  selector: 'app-number-to',
  templateUrl: './number-to.component.html',
  styleUrls: ['./number-to.component.css']
})
export class NumberToComponent implements OnInit {
  constructor(private http: HttpClient) { }

  private controllerName = 'NumberTo';
  private successColor = '#21ba45';
  private errorColor = '#f44336';
  private defaultColor = '#673ab7';

  result = null;
  clicks$ = new Subject<RequestModel>();
  loading = false;
  borderColor = this.defaultColor;

  private handleError(error: HttpErrorResponse) {
    this.loading = false;
    this.borderColor = this.errorColor;
    this.result = error.error;
    return empty();
  }

  ngOnInit() {
    this.clicks$.asObservable()
      .pipe(
        tap(value => {
          this.loading = true;
          this.borderColor = this.defaultColor;
          return value;
        }),
        switchMap(requestModel => {
          const methodName = requestModel.type === 'words' ? 'NumberToWords' : 'NumberToDollars';
          const requestUrl = `${this.controllerName}/${methodName}`;
          return this.http.get(`${requestUrl}?value=${requestModel.value}`, { responseType: 'text' })
            .pipe(
              catchError(error => this.handleError(error))
            );
        })
    )
    .subscribe(result => {
      this.loading = false;
      this.result = result;
      this.borderColor = this.successColor;
    });
  }
}
